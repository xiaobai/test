package com.itheima.service.impl;

import com.itheima.mapper.ResultMapper;
import com.itheima.mapper.StudentMapper;
import com.itheima.mapper.TopicMapper;
import com.itheima.mapper.UserMapper;
import com.itheima.pojo.Student;
import com.itheima.pojo.StudentAndResult;
import com.itheima.pojo.Topic;
import com.itheima.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentMapper studentMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private TopicMapper topicMapper;

    @Autowired
    private ResultMapper resultMapper;
    @Override
    public List<Student> getAllStudent() {

        List<Student> result = studentMapper.getAllStudent();
        return result;
    }

    @Override
    public void alterSudent(Student student) {
        studentMapper.alterStudent(student);
    }

    @Override
    public void deleteById(Integer id) {
        //获取学生名字
        String name = studentMapper.getNameById(id);
        //删除课设结果
        userMapper.deleteBySId(id);
        studentMapper.deleteById(id);
        //删除用户
        userMapper.deleteByName(name);
    }

    @Override
    public Student getStudentByName(String name) {
        Student student = studentMapper.getStudentByName(name);
        return student;
    }

    @Override
    public StudentAndResult getStResultByName(String name) {
        StudentAndResult studentAndResult = studentMapper.getStResultByName(name);
        return studentAndResult;
    }

    @Override
    public List<Topic> getAllTopic() {
        List<Topic> result = studentMapper.getAllTopic();
        return result;
    }

    @Override
    public void selectTopic(Topic topic) {
        //根据课题名字获取课题id
        Integer topicId = topicMapper.getIdByName(topic.getTopicName());
        //根据课题id获取老师id
        Integer teacherId = topicMapper.getTeacherIdById(topicId);
        //把课题id绑定学生
        studentMapper.selectTopic(topicId,teacherId,topic.getName());
    }

    @Override
    public void alterStudentByName(Student student) {
        studentMapper.alterStudentByName(student);
    }
}
