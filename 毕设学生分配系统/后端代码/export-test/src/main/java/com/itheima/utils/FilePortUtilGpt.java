package com.itheima.utils;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.List;

public class FilePortUtilGpt {

    public static <T> void exportExcel(HttpServletResponse response, String title, String[] headers, List<T> dataList, List<String> fields) throws IOException {
        // 创建一个工作簿
        Workbook workbook = new XSSFWorkbook();
        // 创建一个表
        Sheet sheet = workbook.createSheet(title);

        // 创建标题行
        Row headerRow = sheet.createRow(0);
        for (int i = 0; i < headers.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(headers[i]);
        }

        // 填充数据行
        for (int i = 0; i < dataList.size(); i++) {
            Row row = sheet.createRow(i + 1);
            T item = dataList.get(i);

            for (int j = 0; j < fields.size(); j++) {
                Cell cell = row.createCell(j);
                String fieldName = fields.get(j);
                Object value = getFieldValueByName(fieldName, item);
                cell.setCellValue(value != null ? value.toString() : "");
            }
        }

        // 设置响应头，处理文件名的编码问题
        String fileName = URLEncoder.encode(title, "UTF-8");
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + ".xlsx\"");
        OutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();
    }

    //直接获取属性值
//    private static Object getFieldValueByName(String fieldName, Object obj) {
//        try {
//            Field field = obj.getClass().getDeclaredField(fieldName);
//            field.setAccessible(true);
//            return field.get(obj);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }

    //通过实体类中get方式获取属性值
    private static Object getFieldValueByName(String fieldName, Object obj) {
        try {
            // 尝试获取getter方法
            String getter = "get" + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
            Method method = obj.getClass().getMethod(getter);
            return method.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

