package com.itheima.service;

import com.itheima.pojo.Sp;

import java.util.List;

public interface SpService {
    List<Sp> getAllSp();

    void alterSp(Sp sp);

    void deleteById(Integer id);

    void addSp(Sp sp);
}
