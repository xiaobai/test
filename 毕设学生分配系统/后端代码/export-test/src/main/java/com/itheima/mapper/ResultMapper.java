package com.itheima.mapper;

import com.itheima.pojo.ResultPo;
import com.itheima.pojo.ResultVo;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ResultMapper {
//    @Select("select s.name ,r.state , t.name ,topic.name  from student s ,result r,teacher t,topic " +
//            "where s.teacher_id = t.id and s.result_id = r.id and s.topic_id = topic.id ")
//    List<ResultVo> getResult();
    @Select("select s.name as studentName, r.status as resultState, t.name as teacherName, topic.name as topicName from student s, result r, teacher t, topic " +
            "where s.teacher_id = t.id and s.id = r.student_id and s.topic_id = topic.id")
    @Results({
            @Result(property = "name", column = "studentName"),
            @Result(property = "state", column = "resultState"),
            @Result(property = "TeacherName", column = "teacherName"),
            @Result(property = "TopicName", column = "topicName")
    })
    List<ResultVo> getResult();


    @Update("update result set status = #{resultPo.state} where student_id = #{id}")
    void alterResult(@Param("resultPo") ResultPo resultPo, @Param("id") Integer id);

    @Insert("insert into result(student_id) values(#{id})")
    void addInit(Integer id);
}
