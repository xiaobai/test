package com.itheima.mapper;

import com.itheima.pojo.D;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface DMapper {

    @Select("select id, name from d")
    List<D> list();

    @Update("update d set name = #{name} where id = #{id}")
    void alter(D d);

    @Delete("delete from d where id = #{id}")
    void deleteById(Integer id);

    @Insert("insert into d(name) values (#{name})")
    void addD(D d);
}
