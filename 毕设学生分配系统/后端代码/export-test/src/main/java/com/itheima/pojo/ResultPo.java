package com.itheima.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultPo {
    private String name;
    private String state;//0:未批改1:通过2:未通过
    private String TeacherName;
    private String TopicName;
}
