package com.itheima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExportTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExportTestApplication.class, args);
    }

}
