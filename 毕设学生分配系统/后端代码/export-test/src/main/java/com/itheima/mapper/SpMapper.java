package com.itheima.mapper;

import com.itheima.pojo.Sp;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface SpMapper {

    @Select("select sp.id,sp.name,d.name as Dname from sp left join d on sp.D_id = d.id")
    @Results({
            @Result(property = "major", column = "Dname")
    })
    List<Sp> getAllSp();

    @Update("update sp set name = #{name},D_id = #{major} where id = #{id}")
    void alterSp(Sp sp);

    @Delete("delete from sp where id = #{id}")
    void deleteById(Integer id);

    @Insert("insert into sp(name, D_id) values (#{name},#{major})")
    void addSp(Sp sp);
}
