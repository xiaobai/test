package com.itheima.mapper;

import com.itheima.pojo.ResultPo;
import com.itheima.pojo.Student;
import com.itheima.pojo.StudentAndResult;
import com.itheima.pojo.Topic;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface StudentMapper {

    @Select("select s.id,s.name,s.age,s.gender,s2.name as SpName from student s left join webpoject.sp s2 on s.SP_id = s2.id")
    @Results({
            @Result(property = "major", column = "SpName")
    })
    List<Student> getAllStudent();

    @Update("update student set name = #{name}, age = #{age}, gender = #{gender}, SP_id = #{major} where id = #{id}")
    void alterStudent(Student student);

    @Delete("delete from student where id = #{id}")
    void deleteById(Integer id);

    @Select("select name from student where id = #{id}")
    String getNameById(Integer id);

    @Select("select id from student where name = #{name}")
    Integer getIdByName(String name);

    @Select("select s.id,s.name,s.age,s.gender,s2.name as SpName from student s left join webpoject.sp s2 on s.SP_id = s2.id where s.name = #{name}")
    @Results({
            @Result(property = "major", column = "SpName")
    })
    Student getStudentByName(String name);


    @Select("select t.id,t.name,t.age,t.gender,r.status as state,topic.name as TopicName from student s,teacher t,result r,webpoject.topic where s.id = r.student_id and t.id = s.teacher_id and s.topic_id = topic.id and s.name = #{name}")
//    @Results({
//            @Result(property = "state", column = "state"),
//            @Result(property = "TopicName", column = "TopicName")
//    })
    StudentAndResult getStResultByName(String name);

    @Select("select t.name,topic.name as topicName,topic.difficulty,topic.topicInfo from teacher t,topic where t.id = topic.teacher_id")
    List<Topic> getAllTopic();


    @Update("update student set topic_id = #{topicId},teacher_id = #{teacherId} where name = #{name}")
    void selectTopic(@Param("topicId") Integer topicId,@Param("teacherId") Integer teacherId,@Param("name") String name);


    @Update("update student set gender = #{gender},age = #{age},SP_id = #{major} where name = #{name}")
    void alterStudentByName(Student student);

    @Select("select s.name,t.name as topicName,r.status as state from student s,result r,topic t where s.id = r.student_id and s.topic_id = t.id and s.teacher_id = #{id}")
    List<ResultPo> getTopicStateById(Integer id);
}
