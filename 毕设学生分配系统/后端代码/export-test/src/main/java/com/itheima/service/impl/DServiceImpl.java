package com.itheima.service.impl;

import com.itheima.mapper.DMapper;
import com.itheima.pojo.D;
import com.itheima.service.DService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DServiceImpl implements DService {

    //
    @Autowired
    private DMapper dMapper;

    @Override
    public List<D> getAllD() {
        List<D> result = dMapper.list();
        return result;
    }

    @Override
    public void alterD(D d) {
        dMapper.alter(d);
    }

    @Override
    public void deleteById(Integer id) {
        dMapper.deleteById(id);
    }

    @Override
    public void addD(D d) {
        dMapper.addD(d);
    }

}
