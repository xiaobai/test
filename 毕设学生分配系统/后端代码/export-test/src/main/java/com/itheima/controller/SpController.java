package com.itheima.controller;

import com.itheima.pojo.Result;
import com.itheima.pojo.Sp;
import com.itheima.service.SpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
public class SpController {

    @Autowired
    private SpService spService;

    @GetMapping("/admin/p2/sp")
    public Result getAllSp(){
        List<Sp> result = spService.getAllSp();
        return Result.success(result);
    }

    @PutMapping ("/admin/p4")
    public Result alterSp(@RequestBody Sp sp){
        spService.alterSp(sp);
        return Result.success();
    }

    @DeleteMapping("/admin/p4")
    public Result deleteById(@RequestParam Integer id){
        spService.deleteById(id);
        return Result.success();
    }

    @PostMapping("/admin/p4")
    public Result addSp(@RequestBody Sp sp){
        spService.addSp(sp);
        return Result.success();
    }
}
