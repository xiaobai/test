package com.itheima.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ResultVo {
    private String name;
    private String state;//0:未批改1:通过2:未通过
    private String TeacherName;
    private String TopicName;

    // 自定义的 getState 方法
    public String getState() {
        switch (this.state) {
            case "0":
                return "未评分";
            case "1":
                return "通过";
            case "2":
                return "未通过";
            default:
                return "未知状态";
        }
    }
}
