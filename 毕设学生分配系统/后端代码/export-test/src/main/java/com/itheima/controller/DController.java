package com.itheima.controller;

import com.itheima.pojo.D;
import com.itheima.pojo.Result;
import com.itheima.service.DService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
public class DController {

    @Autowired
    private DService dService;

    @GetMapping("/admin/p1/D")
    public Result getAllD(){
        List<D> result = dService.getAllD();
        return Result.success(result);
    }

    @PutMapping("/admin/p3")
    public Result alterD(@RequestBody D d){
        dService.alterD(d);
        return Result.success();
    }

    @DeleteMapping("/admin/p3")
    public Result deleteById(@RequestParam Integer id){
        dService.deleteById(id);
        return Result.success();
    }

    @PostMapping("/admin/p3")
    public Result addD(@RequestBody D d){
        dService.addD(d);
        return Result.success();
    }
}
