package com.itheima.service;

import com.itheima.pojo.ResultPo;
import com.itheima.pojo.ResultVo;

import java.util.List;

public interface ResultService {

    List<ResultVo> getResult();

    List<ResultPo> getResultPo();

    void alterResult(ResultPo resultPo);
}
