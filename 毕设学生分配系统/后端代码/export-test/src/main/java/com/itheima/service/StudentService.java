package com.itheima.service;

import com.itheima.pojo.Student;
import com.itheima.pojo.StudentAndResult;
import com.itheima.pojo.Topic;

import java.util.List;

public interface StudentService {
    List<Student> getAllStudent();

    void alterSudent(Student student);

    void deleteById(Integer id);

    Student getStudentByName(String name);

    StudentAndResult getStResultByName(String name);

    List<Topic> getAllTopic();

    void selectTopic(Topic topic);


    void alterStudentByName(Student student);
}
