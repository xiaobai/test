package com.itheima.controller;


import com.itheima.pojo.Result;
import com.itheima.pojo.User;
import com.itheima.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public Result login(@RequestBody User loginUser){
        String identity = userService.getIdentity(loginUser);
        String name = userService.getNameByAnumber(loginUser.getAnumber());
        return Result.loginSuccess(name,identity);
    }

    @PostMapping("/register")
    public Result register(@RequestBody User loginUser){
        userService.insertUser(loginUser);
        return Result.success();
    }
}
