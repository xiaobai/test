package com.itheima.service.impl;

import com.itheima.mapper.ResultMapper;
import com.itheima.mapper.StudentMapper;
import com.itheima.pojo.ResultPo;
import com.itheima.pojo.ResultVo;
import com.itheima.service.ResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ResultServiceImpl implements ResultService {

    @Autowired
    private ResultMapper resultMapper;

    @Autowired
    private StudentMapper studentMapper;
    @Override
    public List<ResultVo> getResult() {
        List<ResultVo> result = resultMapper.getResult();
        return result;
    }

    public List<ResultPo> getResultPo(){
        List<ResultVo> temp = getResult();
        List<ResultPo> result = new ArrayList<>();
        for(ResultVo t : temp){
            ResultPo resultPo = new ResultPo();
            resultPo.setName(t.getName());
            resultPo.setTeacherName(t.getTeacherName());
            resultPo.setTopicName(t.getTopicName());
            resultPo.setState(t.getState().equals("未评分")  ? "0" : t.getState().equals("通过") ? "1" : "2");
//            System.out.println(t.getState());
            result.add(resultPo);
        }
        return result;
    }

    @Override
    public void alterResult(ResultPo resultPo) {
        //根据名字获取result对应的id(学生)
        Integer id = studentMapper.getIdByName(resultPo.getName());
        System.out.println("id" + id + " " + resultPo.getState());
        resultMapper.alterResult(resultPo,id);

    }
}
