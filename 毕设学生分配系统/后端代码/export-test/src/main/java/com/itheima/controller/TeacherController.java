package com.itheima.controller;

import com.itheima.pojo.Result;
import com.itheima.pojo.ResultPo;
import com.itheima.pojo.Teacher;
import com.itheima.pojo.Topic;
import com.itheima.service.TeacherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    @GetMapping("/admin/p1/teacher")
    public Result getAllTeacher(){
        List<Teacher> result = teacherService.getAllTeacher();
        return Result.success(result);
    }

    @DeleteMapping("/admin/p1")
    public Result deleteById(@RequestParam Integer id){
        teacherService.deleteById(id);
        return Result.success();
    }

    @PutMapping("/admin/p1")
    public Result alterTeacher(@RequestBody Teacher teacher){
        teacherService.alterTeacher(teacher);
        return Result.success();
    }

    @GetMapping("/teacher/p1")
    public Result getTeacherByName(String name){
        Teacher teacher = teacherService.getTeacherByName(name);
        List<Teacher> result = new ArrayList<>();
        result.add(teacher);
        return Result.success(result);
    }

    @GetMapping("/teacher/p2")
    public Result getAllTopicByName(@RequestParam String name){
        List<Topic> result = teacherService.getAllTopicByName(name);
        return Result.success(result);
    }

    @DeleteMapping("/teacher/p2")
    public Result deleteByTopicName(@RequestParam String name){
        teacherService.deleteByTopicName(name);
        return Result.success();
    }

    @PostMapping("/teacher/p2")
    public Result addTopicByTeacherName(@RequestBody Topic topic){
        teacherService.addTopicByTeacherName(topic);
        return Result.success();
    }

    @PutMapping("/teacher/p2")
    public Result teacherAlterTopic(@RequestBody Topic topic){
        teacherService.teacherAlterTopic(topic);
        return Result.success();
    }

    @GetMapping("/teacher/p3")
    public Result getTopicStateByTeacherName(@RequestParam String name){
        List<ResultPo> result = teacherService.getTopicStateByTeacherName(name);
        return Result.success(result);
    }

    @PutMapping("/teacher/p3")
    public Result alterResult(@RequestBody ResultPo resultPo){
        teacherService.alterResult(resultPo);
        return Result.success();
    }

    @PutMapping("/teacher/form")
    public  Result alterTeacherByName(@RequestBody Teacher teacher){
        teacherService.alterTeacherByName(teacher);
        return Result.success();
    }
}
