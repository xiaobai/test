package com.itheima.mapper;

import com.itheima.pojo.Topic;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface TopicMapper {

    @Select("select id from topic where name = #{topicName}")
    Integer getIdByName(String topicName);

    @Select("select teacher_id from topic where id = #{topicId}")
    Integer getTeacherIdById(@Param("topicId") Integer topicId);

    @Select("select name as topicName,difficulty,topicInfo from topic where teacher_id = #{id}")
    List<Topic> getAllByTeacherId(Integer id);

    @Delete("delete from topic where name = #{name}")
    void deleteByName(String name);

    @Insert("insert into topic(name, difficulty, teacher_id, topicInfo) values(#{topic.topicName},#{topic.difficulty},#{id},#{topic.topicInfo})")
    void addTopic(@Param("topic") Topic topic,@Param("id") Integer id);

    @Update("update topic set name = #{topicName},difficulty = #{difficulty},topicInfo = #{topicInfo} where name = #{name}")
    void teacherAlterTopic(Topic topic);
}
