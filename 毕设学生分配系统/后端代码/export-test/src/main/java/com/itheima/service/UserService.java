package com.itheima.service;

import com.itheima.pojo.User;

public interface UserService {
    String getIdentity(User loginUser);

    void insertUser(User loginUser);

    String getNameByAnumber(String anumber);
}
