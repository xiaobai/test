package com.itheima.service.impl;

import com.itheima.mapper.*;
import com.itheima.pojo.ResultPo;
import com.itheima.pojo.Teacher;
import com.itheima.pojo.Topic;
import com.itheima.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    private TeacherMapper teacherMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private TopicMapper topicMapper;

    @Autowired
    private StudentMapper studentMapper;

    @Autowired
    private ResultMapper resultMapper;
    @Override
    public List<Teacher> getAllTeacher() {
        List<Teacher> result = teacherMapper.getAllTeacher();
        return result;
    }

    @Override
    public void deleteById(Integer id) {
        //获取教师名字用于删掉该用户
        String name = teacherMapper.getNameById(id);
        teacherMapper.deleteById(id);
        //删除用户
        userMapper.deleteByName(name);

    }

    @Override
    @Transactional
    public void alterTeacher(Teacher teacher) {

        teacherMapper.alterTeacher(teacher);
    }

    @Override
    public Teacher getTeacherByName(String name) {
        Teacher teacher = teacherMapper.getTeacherByName(name);
        return teacher;
    }

    @Override
    public List<Topic> getAllTopicByName(String name) {
        //根据老师姓名查出老师id
        Integer id = teacherMapper.getIdByName(name);
        //根据老师id查询该老师的所有题目
        List<Topic> result = topicMapper.getAllByTeacherId(id);
        return result;
    }

    @Override
    public void deleteByTopicName(String name) {
        topicMapper.deleteByName(name);
    }

    @Override
    public void addTopicByTeacherName(Topic topic) {
        //根据老师名字查询老师id
        Integer id = teacherMapper.getIdByName(topic.getName());
        //添加题目
        topicMapper.addTopic(topic,id);
    }

    @Override
    public void teacherAlterTopic(Topic topic) {
        topicMapper.teacherAlterTopic(topic);
    }

    @Override
    public List<ResultPo> getTopicStateByTeacherName(String name) {
        //根据姓名查询老师id
        Integer id = teacherMapper.getIdByName(name);
        //学生表联合result表查询
        List<ResultPo> result = studentMapper.getTopicStateById(id);
        return result;
    }

    @Override
    public void alterResult(ResultPo resultPo) {
        //根据学生姓名查询学生id
        Integer id = studentMapper.getIdByName(resultPo.getName());
        //根据学生id修改课题结果
        resultMapper.alterResult(resultPo,id);
    }

    @Override
    public void alterTeacherByName(Teacher teacher) {
        teacherMapper.alterTeacherByName(teacher);
    }


}
