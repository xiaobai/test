package com.itheima.mapper;

import com.itheima.pojo.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {


    @Select("select identity from user where anumber = #{anumber} and password = #{password}")
    String getIdentity(User loginUser);

    @Insert("insert into user(id,username,anumber,password,identity) " +
            "values (null,#{username},#{anumber},#{password},#{identity})")
    void insertUser(User loginUser);

    @Insert("insert into student(name) " +
            "values (#{username})")
    void insertStudent(User loginUser);

    @Insert("insert into teacher(name) " +
            "values (#{username})")
    void insertTeacher(User loginUser);

    @Delete("delete from user where username = #{name}")
    void deleteByName(String name);

    @Select("select id from student where name = #{name}")
    Integer getIdByName(String name);

    @Select("select username from user where anumber = #{anumber}")
    String getNameByAnumber(String anumber);

    @Delete("delete from result where student_id = #{id}")
    void deleteBySId(Integer id);
}
