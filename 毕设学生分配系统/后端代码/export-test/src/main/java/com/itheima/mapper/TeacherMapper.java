package com.itheima.mapper;


import com.itheima.pojo.Teacher;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface TeacherMapper {

    @Select("select teacher.id, teacher.name, age, gender, d.name as Dname from teacher left  join d on teacher.D_id = d.id;")
    @Results({
            @Result(property = "major", column = "Dname")
    })
    List<Teacher> getAllTeacher();

    @Delete("delete from teacher where id = #{id}")
    void deleteById(Integer id);

    @Select("select name from teacher where id = #{id}")
    String getNameById(Integer id);

    @Update("update teacher set name = #{name}, age = #{age}, gender = #{gender}, D_id = #{major} where id = #{id}")
    void alterTeacher(Teacher teacher);

    @Select("select t.id,t.name,t.age,t.gender,d.name as major from teacher t left join d  on t.D_id = d.id where t.name = #{name}")
    Teacher getTeacherByName(String name);

    @Select("select id from teacher where name = #{name}")
    Integer getIdByName(String name);

    @Update("update teacher set age = #{age}, gender = #{gender}, D_id = #{major} where name = #{name}")
    void alterTeacherByName(Teacher teacher);
}
