package com.itheima.service.impl;

import com.itheima.mapper.ResultMapper;
import com.itheima.mapper.UserMapper;
import com.itheima.pojo.User;
import com.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ResultMapper resultMapper;
    @Override
    public String getIdentity(User loginUser) {
        String result = "-1";
        String identity = userMapper.getIdentity(loginUser);
        if(identity != null){
            result = identity;
        }
        return result;
    }

    @Override
    public void insertUser(User loginUser) {
        userMapper.insertUser(loginUser);
        if(loginUser.getIdentity().equals("1")){
            userMapper.insertTeacher(loginUser);
        }
        if(loginUser.getIdentity().equals("2")){
            userMapper.insertStudent(loginUser);
            //根据学生名字查出学生id
            Integer id = userMapper.getIdByName(loginUser.getUsername());
            //初始化学生课设结果
            resultMapper.addInit(id);
        }
    }

    @Override
    public String getNameByAnumber(String anumber) {
        String name = userMapper.getNameByAnumber(anumber);
        return name;
    }
}
