package com.itheima.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Topic {
    private String name;//老师名字|学生名字
    private String topicName;
    private String difficulty;
    private String topicInfo;

}
