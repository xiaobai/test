package com.itheima.controller;

import com.itheima.pojo.Result;
import com.itheima.pojo.Student;
import com.itheima.pojo.StudentAndResult;
import com.itheima.pojo.Topic;
import com.itheima.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping("/admin/p2/student")
    public Result getAllStudent(){
        List<Student> result = studentService.getAllStudent();
        return Result.success(result);
    }

    @PutMapping("/admin/p2")
    public Result alterStudent(@RequestBody Student student){
        studentService.alterSudent(student);
        return Result.success();
    }

    @DeleteMapping("/admin/p2")
    public Result deleteById(@RequestParam Integer id){
        studentService.deleteById(id);
        return Result.success();
    }

    @GetMapping("/student/p1")
    public Result getStudentByName(@RequestParam String name){
        Student student = studentService.getStudentByName(name);
        List<Student> result = new ArrayList<>();
        result.add(student);
        return Result.success(result);
    }

    @GetMapping("/student/p2")
    public Result getStResultByName(@RequestParam String name){
        StudentAndResult studentAndResult = studentService.getStResultByName(name);
        List<StudentAndResult> result = new ArrayList<>();
        if(studentAndResult == null)
            return Result.success();

        result.add(studentAndResult);
        return Result.success(result);
    }

    @GetMapping("/student/p3")
    public Result getAllTopic(@RequestParam String name){
        StudentAndResult studentAndResult = studentService.getStResultByName(name);
        String msg = "success";
        if(studentAndResult != null)
            msg += "已选择";
        List<Topic> result = studentService.getAllTopic();
        return Result.topic(msg,result);
    }

    @PutMapping("/student/p3")
    public Result selectTopic(@RequestBody Topic topic){
        studentService.selectTopic(topic);
        return Result.success();
    }

    @PutMapping("/student/form")
    public Result alterStudentByName(@RequestBody Student student){
        studentService.alterStudentByName(student);
        return Result.success();
    }
}
