package com.itheima.service.impl;

import com.itheima.mapper.SpMapper;
import com.itheima.pojo.Sp;
import com.itheima.service.SpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SpServiceImpl implements SpService {

    @Autowired
    private SpMapper spMapper;
    @Override
    public List<Sp> getAllSp() {
        List<Sp> result = spMapper.getAllSp();
        return result;
    }

    @Override
    public void alterSp(Sp sp) {
        spMapper.alterSp(sp);
    }

    @Override
    public void deleteById(Integer id) {
        spMapper.deleteById(id);
    }

    @Override
    public void addSp(Sp sp) {
        spMapper.addSp(sp);
    }
}
