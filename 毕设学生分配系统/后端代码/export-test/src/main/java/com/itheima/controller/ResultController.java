package com.itheima.controller;

import com.itheima.pojo.Result;
import com.itheima.pojo.ResultPo;
import com.itheima.pojo.ResultVo;
import com.itheima.service.ResultService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
public class ResultController {

    @Autowired
    private ResultService resultService;


    @GetMapping("/admin/p5")
    public Result getResult(){
        List<ResultPo> result = resultService.getResultPo();
        return Result.success(result);
    }

    @PutMapping("/admin/p5")
    public Result alterResult(@RequestBody ResultPo resultPo){
        resultService.alterResult(resultPo);
        return Result.success();
    }
}
