package com.itheima.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {
    private Integer code;//响应码，1 代表成功; 0 代表失败
    private String msg;  //响应信息 描述字符串
    private Object data; //返回的数据

    //增删改 成功响应
    public static Result success(){
        return new Result(1,"success",null);
    }
    //查询 成功响应
    public static Result success(Object data){
        return new Result(1,"success",data);
    }
    //失败响应
    public static Result error(String msg){
        return new Result(0,msg,null);
    }

    public static Result topic(String msg,Object data){
        return new Result(0,msg,data);
    }
    //登录响应
    public static Result loginSuccess(String name ,String identity){
        return new Result(0,identity,name);
    }
    //管理者
    public static Result adminSuccess(){
        return new Result(0,"success",null);
    }

    public static Result adminSuccess(Object data){
        return new Result(0,"success",data);
    }

    public static Result adminError(String msg){
        return new Result(0,msg,null);
    }

}
