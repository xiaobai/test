package com.itheima.service;

import com.itheima.pojo.D;

import java.util.List;


public interface DService {


    List<D> getAllD();

    void alterD(D d);

    void deleteById(Integer id);

    void addD(D d);
}
