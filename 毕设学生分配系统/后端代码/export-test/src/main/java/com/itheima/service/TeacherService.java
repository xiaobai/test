package com.itheima.service;

import com.itheima.pojo.ResultPo;
import com.itheima.pojo.Teacher;
import com.itheima.pojo.Topic;

import java.util.List;

public interface TeacherService {
    List<Teacher> getAllTeacher();

    void deleteById(Integer id);

    void alterTeacher(Teacher teacher);

    Teacher getTeacherByName(String name);

    List<Topic> getAllTopicByName(String name);

    void deleteByTopicName(String name);

    void addTopicByTeacherName(Topic topic);

    void teacherAlterTopic(Topic topic);

    List<ResultPo> getTopicStateByTeacherName(String name);

    void alterResult(ResultPo resultPo);

    void alterTeacherByName(Teacher teacher);
}
