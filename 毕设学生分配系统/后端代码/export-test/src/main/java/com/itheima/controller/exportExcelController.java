package com.itheima.controller;

import com.itheima.pojo.D;
import com.itheima.pojo.Result;
import com.itheima.pojo.ResultVo;
import com.itheima.service.DService;
import com.itheima.service.ResultService;

import com.itheima.utils.FilePortUtilGpt;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

@Slf4j
@RestController
public class exportExcelController {

    //
    @Autowired
    private DService dService;

    @Autowired
    private ResultService resultService;

    @GetMapping("/exportExcel")
    public void exportExcel(HttpServletResponse response){
//        导出的表格名称(文件名）
        String title = "毕设结果";
//        表中第一行表字段
        String[] headers = {"学生姓名","课题结果","老师姓名","课题名字"};

//      从数据库查询出来的结果集
//        List<D> DList = dService.list();
        List<ResultVo> ResultVoList = resultService.getResult();

//       具体需要写入excel需要的那些字段，
//       这些字段从PprojectSalary类中拿，也就是上面的实际数据结果集的泛型
        List<String> list = Arrays.asList("name","state","TeacherName","TopicName");

//        try{
//            FilePortUtil.exportExcel(response,title,headers,ResultVoList,list);
//        }catch (Exception e){
//            log.info("出错");
//        }
        try{
            FilePortUtilGpt.exportExcel(response,title,headers,ResultVoList,list);
        }catch (Exception e){
            log.info("出错");
        }
        System.out.println(ResultVoList);
/**
 * Spring Framework 在处理请求时，
 * 试图将 Result 对象转换为 application/vnd.openxmlformats-officedocument.spreadsheetml.sheet 内容类型，
 * 但没有找到合适的消息转换器。原因是你在 exportExcel 方法中返回了一个 Result 对象，
 * 但响应内容类型已经设置为 Excel 文件类型。*/
//        return Result.success();//
    }
}
