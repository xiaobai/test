import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 引入elementUi组件
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from 'axios'
import store from './store'; // 确保正确导入 store
Vue.config.productionTip = false
Vue.use(ElementUI);

// 定义全局属性
Vue.prototype.$user = {
  username: '',
  identity: ''
};
// 从 localStorage 中获取用户名并赋值给全局属性
const storedUsername = localStorage.getItem('username');
const storedIdentity = localStorage.getItem('identity');
if (storedUsername) {
  Vue.prototype.$user.username = storedUsername;
  Vue.prototype.$user.identity = storedIdentity;
  console.log('username',storedUsername);
  console.log('identity',storedIdentity);
}

// 添加全局导航守卫
router.beforeEach((to, from, next) => {
  const identity = Vue.prototype.$user.identity;
  
  if (to.path.startsWith('/admin') && identity !== '0') {//!==严格不等号 3 != '3' false 3 !== '3' true
    next('/login');
  } else if (to.path.startsWith('/teacher') && identity !== '1') {
    next('/login');
  } else if (to.path.startsWith('/student') && identity !== '2') {
    next('/login');
  } else {
    next(); // 符合身份要求，继续导航
  }

  // 示例：打印当前路由和即将跳转到的路由信息
  console.log('Navigating from:', from.fullPath, 'to:', to.fullPath);
});

axios.interceptors.response.use(response => {
  if (response.data.code === -1) {
    router.push('/login'); // 跳转到登录界面
  }
  return response;
}, error => {
  
  return Promise.reject(error);
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

/**
 * 等同于
 * new Vue({
 *  el : "#app",
 *  router: router,//路由
 *  render : function (h){
 *    return h(App); 
 * }
 * })
 */
//在 Vue 中，render 函数是用来渲染组件的函数，
//它接受一个参数 h，这个 h 是一个函数，也就是 Vue 的渲染函数。h 函数的作用是用来创建虚拟 DOM 元素，然后将其渲染到页面上。
/*
等同于
render: function (h) {
  return h(App);
}
*/