import Vue from 'vue'
import VueRouter from 'vue-router'
// import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/dep',
    name: 'dep',
    component: () => import('../views/tlias/DeptView.vue')
  },
  {
    path: '/emp',
    name: 'emp',
    component: () => import('../views/tlias/EmpView.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login/LoginView.vue')
  },
  {
    path: '/admin',
    redirect: '/admin/p1',//访问该路径是会重定向到p1
    name: 'admin',
    component: () => import('../views/schollJavaWeb/AdminView.vue'),
    children:[
      {
        path: '/admin/p1',
        name: 'adminP1',
        component: () => import('../views/schollJavaWeb/adminChild/P1View.vue')
      },
      {
        path: '/admin/p2',
        name: 'adminP2',
        component: () => import('../views/schollJavaWeb/adminChild/P2View.vue')
      },
      {
        path: '/admin/p3',
        name: 'adminP3',
        component: () => import('../views/schollJavaWeb/adminChild/P3View.vue')
      },
      {
        path: '/admin/p4',
        name: 'adminP4',
        component: () => import('../views/schollJavaWeb/adminChild/P4View.vue')
      },
      {
        path: '/admin/p5',
        name: 'adminP5',
        component: () => import('../views/schollJavaWeb/adminChild/P5View.vue')
      }
    ]
  },
  {
    path: '/teacher',
    redirect: '/teacher/p1',//访问该路径是会重定向到p1
    name: 'teacher',
    component: () => import('../views/schollJavaWeb/TeacherView.vue'),
    children:[
      {
        path: '/teacher/p1',
        name: 'teacherP1',
        component: () => import('../views/schollJavaWeb/teacherChild/P1View.vue')
      },
      {
        path: '/teacher/p2',
        name: 'teacherP2',
        component: () => import('../views/schollJavaWeb/teacherChild/P2View.vue')
      },
      {
        path: '/teacher/p3',
        name: 'teacherP3',
        component: () => import('../views/schollJavaWeb/teacherChild/P3View.vue')
      },
      {
        path: '/teacher/p4',
        name: 'teacherP4',
        component: () => import('../views/schollJavaWeb/teacherChild/FormView.vue')
      },
    ]
  },
  {
    path: '/student',
    redirect: '/student/p1',//访问该路径是会重定向到p1
    name: 'student',
    component: () => import('../views/schollJavaWeb/StudentView.vue'),
    children:[
      {
        path: '/student/p1',
        name: 'studentP1',
        component: () => import('../views/schollJavaWeb/studentChild/P1View.vue')
      },
      {
        path: '/student/p2',
        name: 'studentP2',
        component: () => import('../views/schollJavaWeb/studentChild/P2View.vue')
      },
      {
        path: '/student/p3',
        name: 'studentP3',
        component: () => import('../views/schollJavaWeb/studentChild/SelectTopicView.vue')
      },
      {
        path: '/student/p4',
        name: 'studentP4',
        component: () => import('../views/schollJavaWeb/studentChild/FormView.vue')
      },
      {
        path: '/empty',
        name: 'empty',
        component: () => import('../views/schollJavaWeb/empty/EmptyView.vue')
      }
    ]
  },
  {
    path: '/',
    redirect: 'login'
  },
  {
    path: '/errorPath',
    name: 'errorPath',
    component: () => import('../views/errorPath/ErrorPathView.vue')
  },
  {
    path: '*',
    redirect: 'errorPath'
  }
]

const router = new VueRouter({
  routes
})

export default router
