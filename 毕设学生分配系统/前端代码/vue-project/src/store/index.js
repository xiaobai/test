import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    username: '',
    password: ''
  },
  mutations: {
    setUsername(state, username) {
      state.username = username;
    },
    setPassword(state, password) {
      state.password = password;
    }
  },
  actions: {
    // 你可以在这里定义异步操作
  },
  // getters: {
    
  // },
  modules: {
    app,
    settings,
    user
  },
  getters
})

export default store
