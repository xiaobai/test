from flask import Flask
from flask import send_from_directory
from flask import request
from flask_cors import CORS
import mysql.connector

app = Flask(__name__)
CORS(app)

from flask import request

@app.route('/login', methods=['GET', 'POST'])
def parse_request():
    print(request.form.get('name'))
    data = request.json
    name = data.get('name')
    password = data.get('password')
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="123456",
        database="webdb"
    )
    mycursor = mydb.cursor()
    sql = "SELECT * FROM login WHERE name = %s AND password = %s"
    val = (name, password)
    mycursor.execute(sql, val)
    result = mycursor.fetchall()
    
    if result:
        return {'message': '<p>登录成功</p>', 'code': 'SUCCESS'}
    else:
        return {'message': '<p>用户名或密码错误</p>', 'code': 'ERROR'}
    #return "<p>登录成功</p>"
@app.route('/register', methods=['GET', 'POST'])
def register():
    data = request.json
    print(data)
    sql = "INSERT INTO login (name, password, email, sex, age, introduction) VALUES (%s, %s, %s, %s, %s, %s)"
    sex = 0
    if data["sex"] == "on":
        sex = 1
    val = (data["name"], data["password"], data["email"], data["sex"], data["age"], data["introduction"])
    mydb = mysql.connector.connect(
      host="localhost",
      user="root",
      password="123456",
      database="webdb"
    )
    mycursor = mydb.cursor()
    mycursor.execute(sql, val)
    mydb.commit()
    return {'message': '<p>注册成功</p>', 'code': 'SUCCESS'}

@app.route("/hi")
def hello_world():
    return "<p>Hello, World!</p>"
@app.route('/web/<path:path>')
def static_file(path):
    return send_from_directory('web', path)

